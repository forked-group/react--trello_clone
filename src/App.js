import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
// import { Switch, Route } from 'react-router-dom';
import NavBar from './components/NavBar';
import Board from './components/Board';
// import Default from './components/Default';
// require('dotenv').config();

// class App extends Component {
//   render() {
//     return (
//       <React.Fragment>
//         <NavBar />
//         <Switch>
//           <Route exact path='/' component={Board} />
//           <Route component={Default} />
//         </Switch>
//       </React.Fragment>
//     );
//   }
// }

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <NavBar />
        <Board />
      </React.Fragment>
    );
  }
}

export default App;
