import React from 'react';
import styled from 'styled-components';
import Modal from 'react-responsive-modal';
import ModalWrapper from './cardsComponents/ModalWrapper';
export default function Card(props) {
  const cards = props.cards;
  return (
    <React.Fragment>
      {cards.map(card => {
        return (
          <Cards key={card.id}>
            <span
              onClick={() => {
                props.getCardsDetails(card.id, card.name);
                props.openModalHandler();
              }}
            >
              {card.name}
            </span>
            <i
              className='fas fa-trash'
              onClick={() => props.deleteCard(card.id, props.listID)}
            ></i>
          </Cards>
        );
      })}
      <Modal open={props.isShowing} onClose={() => props.closeModalHandler()}>
        <ModalWrapper />
      </Modal>
    </React.Fragment>
  );
}
const Cards = styled.div`
  padding: 1em;
  display: flex;
  justify-content: space-between;
  background-color: #fff;
  border-radius: 0.5em;
  margin-bottom: 1em;
  word-break: break-all;
`;
