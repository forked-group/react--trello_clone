import React, { Component } from 'react';
import { DataConsumer } from '../../context';
import CheckItem from './CheckItem';
import InputFiled from '../inputFiled';
export default class Modal extends Component {
  render() {
    return (
      <React.Fragment>
        <DataConsumer>
          {value => {
            return (
              <React.Fragment>
                <div
                  style={{ display: 'flex', justifyContent: 'space-between' }}
                >
                  <h2>{value.cardName}</h2>
                  {value.inputShowCheck ? (
                    <InputFiled
                      name={'checkList'}
                      keyPressed={value.keyPressed}
                    />
                  ) : null}
                  <button
                    className='btn btn-dark'
                    style={{ marginRight: '2em' }}
                    data-type={'checkList'}
                    onClick={() => value.showInput(event)}
                  >
                    Add CheckList
                  </button>
                </div>
                {/*   mapping and creating checkItems*/}
                {value.cards.map(checkList => (
                  <div className='checkLists' key={checkList.id}>
                    <h5>{checkList.name}</h5>
                    <div className='checksWrapper'>
                      <CheckItem
                        list={checkList.checkItems}
                        deleteCheckItem={value.deleteCheckItem}
                        checkListID={checkList.id}
                        updateCheckItem={value.updateCheckItem}
                      />
                    </div>
                    <div className='cards-button'>
                      <button
                        className='btn btn-dark'
                        style={{ marginRight: '2em' }}
                        data-type={'checkItem'}
                        data-id={checkList.id}
                        onClick={() => value.showInput(event)}
                      >
                        + Add Item
                      </button>
                      {value.inputShowItem &&
                      value.checkListId === checkList.id ? (
                        <InputFiled
                          name={'checkItem'}
                          keyPressed={value.keyPressed}
                        />
                      ) : null}
                      <button
                        className='btn btn-dark'
                        onClick={() => value.deleteCheckList(checkList.id)}
                      >
                        &minus; Delete CheckList
                      </button>
                    </div>
                  </div>
                ))}
              </React.Fragment>
            );
          }}
        </DataConsumer>
      </React.Fragment>
    );
  }
}
